package com.softtek.academia.JdbcExample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.softtek.academia.dao.ColorJDBCImpl;
import com.softtek.academia.model.ColorDTO;
import com.softtek.academia.service.ColorServiceImpl;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	System.out.println("MySQL JDBC Connection");
    	ColorJDBCImpl colorDao = new ColorJDBCImpl();
    	ColorServiceImpl colorService
    		= new ColorServiceImpl(colorDao);
    	
    	List<ColorDTO> colors = colorService.getAll();
    	colors.forEach(System.out::println);
/*
        List<Object> result = new ArrayList<>();

        String SQL_SELECT = "Select * from COLOR";
        
    	// auto close connection
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234")) {

            if (conn != null) {
                System.out.println("Connected to the database!");
                
                PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);

                ResultSet resultSet = preparedStatement.executeQuery();
                ArrayList<Color> Colores = new ArrayList<Color>();
                
                
                while (resultSet.next()) {
                    long id = resultSet.getLong("COLORID");
                    String name = resultSet.getString("NAME");
                    String hexValue = resultSet.getString("HEXVALUE");
                    
                    MySupplier supplier = (i, n, c) -> new Color(i, n, c);
                    Color color = supplier.get(id, name, hexValue);
                    Colores.add(new Color((int) id,name,hexValue));
                    
                }
                */
        /*        
                //Search a color based on their ID Function
                BiFunction<List<Long>,Long,Color> searchFunction = (list,l) -> (list,l);
        		
                for(Color c : list) {
                	if(l == c.getColorId()) {
                		return null;
                	}
                }
                
        		//HexColor is inside the list ? Test
        		BiPredicate <List<Color>, String> hexColorCheck = (1,s) -> {
        			for(Color c : 1) {
        				if(c.getHexValue().equals(s)) {
        					return false;
        				}
        			}
        		};
        		
        		BiConsumer<List<Color>, String> printByValue = (1,v) -> {
        			for(Color col:l) {
        				switch(v) {
        				case "id": 
        					System.out.printl(col.getId());
        				
        				case "name": 
        					System.out.printl(col.getName());
        					
        				default: 
        					System.out.printl(col.getHexValue());
        				}
        			}
        		}        
        		printByValue.accept(result, "name");
        		printByValue.accept(result, "");
        		
        		/*
                
        		//Supplier function generates a Color Object using name and HexValue
                Predicate <String> predicate = (hexColor) -> hexColor !=null ;
        		System.out.println(predicate.test(null));//false
                
        		//Print color list and the parameter you specified "Consumer"
        		Consumer <Integer> myConsumer = System.out::println;
        		myConsumer.accept(5);
                */
     /*           
                Colores.stream() //List<Colr>
                	.map(Color::getHexValue)//List<Long>.
                	.forEach(System.out::println);
                
                Long sumatoria = 
                Colores.stream()
                	.map(Color::getId)//List<Long>
                	.reduce(0L, (id1,id2) -> id1 + id2);
                
                
                List <String> NombreColores =
                	Colores.stream().map(Color::getHexValue).collect(Collectors.toList());
                
                NombreColores.stream().filter(str -> str.startsWith("A")) .forEach(System.out::println); 
                
            	//Colores.stream().map(Color::getHexValue).filter(f -> f.startsWith( "A" ).forEach(System.out::println));
*/
       /*         	
                	System.out.println();
                
                conn.close();
           
            } else {
                System.out.println("Failed to make connection!");
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }*/   
    }
}
