package com.softtek.academia.JdbcExample;

@FunctionalInterface
public interface MySupplier<C extends Color> {
	
	public Color get(long id,String name, String hexValue);
	
	
}
