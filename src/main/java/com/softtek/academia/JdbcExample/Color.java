package com.softtek.academia.JdbcExample;

public class Color{
	long id;
	String name;
	String hexValue;
	
	
	
	
	public Color(long id, String name, String hexValue) {
		super();
		this.id = id;
		this.name = name;
		this.hexValue = hexValue;
	}
	
	
	public long getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getHexValue() {
		return hexValue;
	}
	public void setHexValue(String hexValue) {
		this.hexValue = hexValue;
	}

	
	
}


