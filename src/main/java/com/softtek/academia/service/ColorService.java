package com.softtek.academia.service;

import java.util.List;

import com.softtek.academia.dao.ColorDAO;
import com.softtek.academia.model.ColorDTO;

public interface ColorService {
	
	List<ColorDTO> getAll();
	
	ColorDTO getById(long id);

	List<ColorDTO> getFilterList();
}
