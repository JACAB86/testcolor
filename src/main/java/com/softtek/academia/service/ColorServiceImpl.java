package com.softtek.academia.service;

import java.util.List;
import java.util.stream.Collectors;

import com.softtek.academia.dao.ColorDAO;
import com.softtek.academia.model.ColorDTO;

public class ColorServiceImpl implements ColorService {

		private ColorDAO colorDao;

		public List<ColorDTO> getAll() {
			return colorDao.getAll();
		}

		public ColorServiceImpl(ColorDAO colorDao) {
			this.colorDao = colorDao;
		}

		@Override
		public ColorDTO getById(long id) {
			// TODO Auto-generated method stub
			return colorDao.getById(id);
		}

		@Override
		public List<ColorDTO> getFilterList() {
			// TODO Auto-generated method stub
			List<ColorDTO>  colors = colorDao.getAll();
			/*colors.stream().filter((c)-> !c.getName()
			.equals("Black").collect(Collectors.toList()));
			*/
			colors.removeIf((c)-> c.getName().equals("Black"));
			return colors;
		}
		
		
}
