package com.softtek.academia.dao;

import java.util.List;

import com.softtek.academia.model.ColorDTO;


public interface ColorDAO{
	List<ColorDTO> getAll();

	ColorDTO getById(long id);
}
	

