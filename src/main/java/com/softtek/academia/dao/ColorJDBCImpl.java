package com.softtek.academia.dao;

 

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.academia.model.ColorDTO;

 

public class ColorJDBCImpl implements ColorDAO {

	static Connection CONECTION;
	
	private static Connection getConnection() {
		if(CONECTION == null) {
		try {
			CONECTION = DriverManager.getConnection(
	                "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234");
		} catch (SQLException e) {
	            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
			}
		}
	return CONECTION;
	}
	
	public static void closeConnection() {
		try {
			getConnection().close();
		}	catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
		}
		
	}
	
	
	private PreparedStatement getReadyForQuery(String query) throws SQLException {
		PreparedStatement preparedStatement = null;
		preparedStatement = ColorJDBCImpl.getConnection().prepareStatement(query);
		 
		 return preparedStatement;
	}

    @Override
    public List<ColorDTO> getAll() {
        System.out.println("MySQL JDBC Connection");
        List<ColorDTO> result = new ArrayList<>();
        String SQL_SELECT = "Select * from COLOR";
        
        // auto close connection
        try {
        	PreparedStatement preparedStatement = getReadyForQuery(SQL_SELECT);
        	ResultSet resultSet = preparedStatement.executeQuery();
        	
        	while (resultSet.next()) {
                    long id = resultSet.getLong("COLORID");
                    String name = resultSet.getString("NAME");
                    String hexValue = resultSet.getString("HEXVALUE");
                    //System.out.println("ID: " + id + " color's name: " + name + " hex's value: " + hexValue);
                    ColorDTO color = new ColorDTO(id,name,hexValue);
                    result.add(color);
                }
          
        }catch (Exception e) {
           e.printStackTrace();
        }
        return result;
 }

	@Override
	public ColorDTO getById(long id) {
		// TODO Auto-generated method stub
		String SQL_SELECT = "Select * from COLOR where colorId = ?";
		ColorDTO color = null;
        
        try {
        	PreparedStatement preparedStatement = getReadyForQuery(SQL_SELECT);
        	preparedStatement.setLong(1, id);
        	ResultSet resultSet = preparedStatement.executeQuery();
        	
        	while (resultSet.next()) {
                    long Colorid = resultSet.getLong("COLORID");
                    String name = resultSet.getString("NAME");
                    String hexValue = resultSet.getString("HEXVALUE");
                    //System.out.println("ID: " + id + " color's name: " + name + " hex's value: " + hexValue);
                    color = new ColorDTO(Colorid,name,hexValue);
                    
                }
          
        }catch (Exception e) {
           e.printStackTrace();
        }
        return color;
	}

 

}