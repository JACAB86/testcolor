package com.softtek.academia.dao;

//import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;


import java.util.List;

import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.softtek.academia.model.ColorDTO;

public class ColorDaoTest {

	private ColorDAO colorDao;
	
	@BeforeEach
	public void initJDBC() {
		this.colorDao = new ColorJDBCImpl();
	}
	
	@Test
	public void testGetAllColors() {
		//Setup
		ColorDTO color = new ColorDTO(1,"White","#FFFFFF");
		//Execute
		List<ColorDTO> colors = colorDao.getAll();
		//Verify
		assertNotNull(colors, "ColorList is empty");
		ColorDTO actualColor = colors.get(0);
		assertEquals(color.getName(), actualColor.getName(),"Colors don't match");
	}
	
	@Test
	public void testGetColorById() {
		//Setup
		long id = 2;
		ColorDTO expectedColor = new ColorDTO(2,"Silver","#C0C0C0");
		
		//Execute
		ColorDTO actualColor = colorDao.getById(id);
		
		//Verify
		assertNotNull(actualColor, "ColorList is empty");
		assertEquals(expectedColor.getName(), actualColor.getName(),"Colors don't match");
		
	}
}
